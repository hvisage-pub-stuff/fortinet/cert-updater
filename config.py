#! /usr/bin/env python3

from datetime import date
from datetime import datetime

# Log file path - edit these to your liking
logfile_path = "/home/matt/fgt-cert-updater/"
logfile_name = "certupdate{}.log".format(date.today())

# Certificate related variables
cert_name = "uv-certbot-ssl{}".format(date.today())
cert_fullchain_file = "/etc/letsencrypt/live/ultraviolet.network/fullchain.pem"
cert_privkey_file = "/etc/letsencrypt/live/ultraviolet.network/privkey.pem"

# Fortigates to be updated - this is a python list, to add a new element just add a comma
# to the end of the } and start with a new dictionary
#
# example my_gates = [
# {
#     "hostname": "gate1.domain.local",
#     "key":"your_key_here",
#     "port": "8443",
#     "VerifyCert": False
# },{
#       "hostname": "gate2.domain.local",
#       "key":"your_key_here",
#       "port": "8443",
#       "VerifyCert": False
# }
# ]

my_gates = [
{
    "hostname": "uv-gate.ultraviolet.network",
    "host_ip":"10.220.47.1",
    "key":"h19HNb9kGNQzzxrsQQ8prcjfct1kw1",
    "port": "8443",
    "VerifyCert": False
},
{
    "hostname": "uvaz-gate.ultraviolet.network",
    "host_ip": "172.20.20.2",
    "key": "wqfpjbfddNmbNbHbN5qQtNxn46kwQ7",
    "port": "8443",
    "VerifyCert": False
}]

# Creating the JSON for the cert creation
cert_post_body = {
    "type": "regular",
    "certname": cert_name,
    "key_file_content": "",
    "scope": "global",
    "file_content": ""
}
# Creating the JSON for for assigning the cert as the system/admin cert
cert_system_assignment = {
    "admin-server-cert": cert_name
}

