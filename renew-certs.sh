#!/bin/bash

# Running the certbot renewal script
cert_renewal=`certbot renew`

# Checking to see if any renewals were done
if [[ $cert_renewal == *"No renewals were attempted"* ]] 
then
  echo "No Certs were up for renewal. Aborting"
else
   python3.6 /root/scripts/uv-certbot-ssl.py
fi




